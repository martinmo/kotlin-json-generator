@file:Suppress("MagicNumber")

package mm.json.generator

import mm.json.generator.OutputFormat.JSON_ARRAY_WITH_NEWLINE
import kotlin.random.Random


fun main() {
    Generator(
        target = System.out,
        times = 1000,
        outputFormat = JSON_ARRAY_WITH_NEWLINE
    ).generate {

        "type" value "user"

        "firstName" value randomString(8)
        "lastName" value randomString(10)

        "age" value Random.nextInt(18, 99)
        "score" value Random.nextDouble(0.0, 1.0)

        "address" obj {
            "street" value randomString(10)
            "city" value randomString(10)
            "postcode" value Random.nextInt(10000, 99999)
        }

        "tags" array {
            randomRange(1, 5).forEach { _ ->
                append(randomString(8))
            }
        }

        "ids" array {
            Random.nextInt(5).times {
                append(Random.nextInt())
            }
        }

        "items" array {
            10.times {
                append {
                    "id" value 1
                    "name" value "name"
                }
            }
        }
    }
}
