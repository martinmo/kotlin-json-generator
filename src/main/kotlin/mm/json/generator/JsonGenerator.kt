package mm.json.generator

import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerator
import mm.json.generator.OutputFormat.JSON_ARRAY
import java.io.OutputStream
import kotlin.random.Random

data class Generator(
    val target: OutputStream,
    val times: Int = 1,
    val outputFormat: OutputFormat = JSON_ARRAY
) {
    fun generate(builder: JsonBuilder.() -> Unit) {
        val jsonFactory = JsonFactory()
        jsonFactory.configure(JsonGenerator.Feature.STRICT_DUPLICATE_DETECTION, true)
        val jg: JsonGenerator = jsonFactory.createGenerator(target, JsonEncoding.UTF8)
        outputFormat.write(jg, builder, target, times)
        jg.flush()
    }
}

enum class OutputFormat : ResultWriter {
    JSON_ARRAY {
        override fun write(jg: JsonGenerator, builder: JsonBuilder.() -> Unit, os: OutputStream, times: Int) {
            jg.writeStartArray()
            (1..times).forEach { _ ->
                jg.writeStartObject()
                builder(JsonBuilder(jg))
                jg.writeEndObject()
            }
            jg.writeEndArray()
        }
    },
    LINE_SEPARATED {
        override fun write(jg: JsonGenerator, builder: JsonBuilder.() -> Unit, os: OutputStream, times: Int) {
            (1..times).forEach { _ ->
                jg.writeStartObject()
                builder(JsonBuilder(jg))
                jg.writeEndObject()
                jg.flush()
                os.write("\n".toByteArray())
            }
        }
    },
    JSON_ARRAY_WITH_NEWLINE {
        override fun write(jg: JsonGenerator, builder: JsonBuilder.() -> Unit, os: OutputStream, times: Int) {
            jg.writeStartArray()
            (1..times).forEach { _ ->
                jg.writeStartObject()
                builder(JsonBuilder(jg))
                jg.writeEndObject()
                jg.flush()
                os.write("\n".toByteArray())
            }
            jg.writeEndArray()
        }
    }
}

interface ResultWriter {
    fun write(jg: JsonGenerator, builder: JsonBuilder.() -> Unit, os: OutputStream, times: Int)
}

data class JsonBuilder(val jg: JsonGenerator) {

    infix fun String.value(string: String) {
        jg.writeStringField(this, string)
    }

    infix fun String.value(num: Number) {
        jg.writeFieldName(this)
        writeNumber(num)
    }

    private fun writeNumber(num: Number) {
        when (num) {
            is Int -> jg.writeNumber(num)
            is Double -> jg.writeNumber(num)
            is Float -> jg.writeNumber(num)
            else -> throw IllegalArgumentException("number not supported: ${num.javaClass}")
        }
    }

    infix fun String.obj(inner: JsonBuilder.() -> Unit) {
        jg.writeObjectFieldStart(this)
        inner(this@JsonBuilder)
        jg.writeEndObject()
    }

    infix fun String.array(inner: JsonBuilder.() -> Unit) {
        jg.writeArrayFieldStart(this)
        inner(this@JsonBuilder)
        jg.writeEndArray()
    }

    infix fun append(num: Number) {
        writeNumber(num)
    }

    infix fun append(string: String) {
        jg.writeString(string)
    }

    infix fun append(inner: JsonBuilder.() -> Unit) {
        jg.writeStartObject()
        inner(this)
        jg.writeEndObject()
    }
}

fun randomString(length: Int, charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')): String {
    val intRange: IntRange = 1..length
    return intRange.map { charPool[Random.nextInt(0, charPool.size)] }.joinToString("")
}

fun Int.times(consumer: (Int) -> Unit) {
    (0..this).forEach(consumer)
}

fun randomRange(start: Int, maxLength: Int): IntRange = start..Random.nextInt(start, maxLength)
