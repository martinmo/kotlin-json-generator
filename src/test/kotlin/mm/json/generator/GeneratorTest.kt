package mm.json.generator

import io.kotlintest.Description
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import java.io.ByteArrayOutputStream

internal class GeneratorTest : StringSpec() {

    val output = ByteArrayOutputStream()

    override fun beforeTest(description: Description) {
        output.reset()
    }

    init {
        "can generate simple object with string value" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY).generate {
                "key" value "value"
            }

            output.asString() shouldBe """[{"key":"value"}]"""
        }

        "can generate simple object with int, float and double values" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY).generate {
                "int" value 1
                "float" value 1.1.toFloat()
                "double" value 2.2
            }

            output.asString() shouldBe """[{"int":1,"float":1.1,"double":2.2}]"""
        }

        "can generate nested objects" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY).generate {
                "a" obj {
                    "b" value 1
                }
            }

            output.asString() shouldBe """[{"a":{"b":1}}]"""
        }

        "can generate array with strings" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY).generate {
                "a" array {
                    append("1")
                    append("2")
                }
            }

            output.asString() shouldBe """[{"a":["1","2"]}]"""
        }

        "can generate array with numbers" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY).generate {
                "a" array {
                    append(1)
                    append(1.1)
                }
            }

            output.asString() shouldBe """[{"a":[1,1.1]}]"""
        }

        "can generate array with object" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY).generate {
                "a" array {
                    append {
                        "inner" value 1
                    }
                }
            }

            output.asString() shouldBe """[{"a":[{"inner":1}]}]"""
        }

        "can format output per line" {
            Generator(output, outputFormat = OutputFormat.LINE_SEPARATED, times = 2).generate {
                "a" value "b"
            }

            val expectedOutput = """
            {"a":"b"}
             {"a":"b"}

            """.trimIndent()

            output.asString() shouldBe expectedOutput
        }

        "can format output json array per line" {
            Generator(output, outputFormat = OutputFormat.JSON_ARRAY_WITH_NEWLINE, times = 2).generate {
                "a" value "b"
            }

            val expectedOutput = """
            [{"a":"b"}
            ,{"a":"b"}
            ]""".trimIndent()

            output.asString() shouldBe expectedOutput
        }

        "can execute demo without exceptions" {
            main()
        }
    }
}

fun ByteArrayOutputStream.asString(): String = this.toByteArray().toString(Charsets.UTF_8)