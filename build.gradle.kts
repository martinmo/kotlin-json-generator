import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.10"
    application
    id("io.gitlab.arturbosch.detekt").version("1.0.0-RC12")
}

group = "mm.json.generator"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("com.fasterxml.jackson.core:jackson-core:2.9.8")
    testCompile("org.jetbrains.kotlin:kotlin-reflect:1.3.10") // override kotlintest's older version
    testCompile("io.kotlintest:kotlintest-runner-junit5:3.1.11")
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

detekt {
    toolVersion = "1.0.0-RC12"
    filters = ".*/Demo*"
}

application {
    mainClassName = "mm.json.generator.DemoKt"
}

defaultTasks = listOf("clean", "test", "detekt", "build")
